.PHONY: all install post-install clean

all:
	$(MAKE) -C linux all
install:
	$(MAKE) -C linux install
	$(MAKE) -C noarch install

post-install:
	$(MAKE) -C linux post-install

clean:
	$(MAKE) -C linux clean
