shelli - a shell environment (scripts/config/etc) to make a unix system more usable like a smart phone.

Copyright 2013-2020 Craig Comstock <craig@unreasonablefarm.org>

This is primarily targeted towards phones running a Linux based operating system
such as PostmarketOS or Termux on Android.

# Build Dependencies
  - on postmarketos, apk add alpine-sdk linux-headers

# Runtime Dependencies
  - python3
  - py3-gobject3
  - kbd kbd-misc (for large font)
  - bash (for better command line completion of contact short-cuts)
  - ofono and ofonoctl
  - alsa-utils (alsaucm) for management of alsa use case (earpiece, headphones, speaker)
  - espeak for notifications

NOTE: The termux port is not yet complete so for now only postmarketos/linux is supported.

# Installation

As part of a postmarketos build (pmbootstrap):

  Select shelli for the UI.
  This will also include [gesture](https://gitlab.com/unrznbl/gesture/) as the primary means of input with the framebuffer console
  
  Another alternative is to use fbkeyboard UI and add shelli as an extra package.

On postmarketos:

  sudo apk add shelli

Manual build:

  sudo make install

Ensure that your user account is added to the wheel group for the brightness command to work properly.

# Files
- ~/Contacts - each line is <phone number><tab><any contact info like name etc>
- /var/spool/sms - all incoming and outgoing text messages are logged here
- /var/spool/calls - calls are logged here

# User Command Summary
- battery - prints out battery capacity percent, -v show more info if available
- brightness <percent> - sets brightness, with no <percent> shows current value
- call <contact search or number> - makes a phone call
- contacts <name or part of name> - search ~/Contacts for contact info
- messages - print out all sms messages
- search <contact search>
  - if only one contact then show all messages to/from that contact
  - if more than one, print them all out
  - if one contact and more args, send text message
- text <contact search> <message>
  - send sms
  - if no <message> then interactive line by line sending
- ring <type of notification> <from whom> - notifies
- say <text> - use espeak to say something, if no <text> then interactive line-by-line output
- usb-internet - setup phone side of usb internet setup
- airplane - use ofonoctl to make modem offline
- answer - answer an incoming phone call
- calls - show current active calls
- dial <number> - start a phone call
- hangup - hangup the current phone call
- sms <number> <message> - send an sms message
- earpiece - sets earpiece audio output
- headphones - sets headphone audio output
- speaker - sets speaker audio output
(all of the above set the same default mic input)


# Internal Commands
- screen_on_off - toggles brightness between 0 and current setting in /var/lib/shelli/brightness.conf (requires root to start/stop gesture service)
- largefont - set large font
- gesture2tty - setup running gesture service
- onkey -e <eventcode> -d <device> -c <command> - run command when eventcode is pressed on input device
- vibrate <event path> - short vibrate (works on harpia for now)
- calld - ofono python script to listen for calls and ring and log to /var/spool/calls
- smsd - ofono python script to listen for sms and ring and log to /var/spool/sms
- shelli-audio-discovery - determines audio capabilities and names of alsaucm to use later
- shelli-audio-update - read /var/lib/shelli/audio.conf, overlay with NEW_x env vars and update audio config with alsaucm

# TODO
- volume keys to change notification from silent to loud
- headphones inserted detection to switch to headphones audio output
- dtmf to handle in-call menus

# Known Issues
- screen_on_off needs su access to stop gesture service
- logging out on-screen kills gesture. power button off then on restores it
- text/sms/call doesn't check modem status for online/network

# Device Specific Notes
- hammerhead
  - no audio
  - no battery status
- harpia
  - no mobile data
- klte
  - haven't tested modem
  - brightness doesn't work so powerkey has no effect
  - mainline has a freeze with inactivity after about 5 minutes
- librem5
  - ofono not supported probably, either make that work or add modemmanager as an option to shelli
  - touchscreen/gesture doesn't seem to work (odd events maybe?)
